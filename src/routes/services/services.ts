import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import _ from 'lodash';
import { DateTime } from 'luxon';

import { ServiceModels } from '../../models/services/services';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db = fastify.db;
  const serviceModels = new ServiceModels();

  fastify.get('/patient', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const _query: any = request.query;
      const { limit, offset, query } = _query;
      const _limit = limit || 20;
      const _offset = offset || 0;
      const data: any = await serviceModels.patient(db,_query.an,_limit,_offset);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/opdReview', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      const _query: any = request.query;
      const { limit, offset, query } = _query;
      const _limit = limit || 20;
      const _offset = offset || 0;
      const data: any = await serviceModels.opdReview(db,_query.an,_limit,_offset);

      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  done();

} 
